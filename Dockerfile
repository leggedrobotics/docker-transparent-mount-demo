ARG ROS_DISTRO=melodic
ARG FROM=ros:$ROS_DISTRO

FROM $FROM

RUN apt-get update && \
  apt-get install -y zsh && \
  rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh 
ENTRYPOINT ["/entrypoint.sh"]

CMD []

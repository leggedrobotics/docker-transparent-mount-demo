#!/bin/bash

ROS_DISTRO=${1:-melodic}

docker build --build-arg ROS_DISTRO=$ROS_DISTRO -t mount-demo-$ROS_DISTRO .

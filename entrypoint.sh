#!/bin/bash

set -m

echo "$HOST_USERNAME ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers

if [ -z "$1" ]; then
  su - $HOST_USERNAME
else
  echo "$@"
  su - $HOST_USERNAME -c "$@"
fi


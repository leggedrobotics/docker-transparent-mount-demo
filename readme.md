Transparent mounting of volumes in Docker demo
====

An effort to make the experience of working in a Docker container as transparent as possible for the user. 

- Copy /etc/shadow, /etc/passwd, /etc/group to container
- Mount $HOME
- Set-up user switching

```bash
./run.sh noetic
./run.sh melodic lsb_release -a
./run.sh foxy ls
```

The `run.sh` script configures `docker run` and passes env-vars. 
The `entrypoint.sh` receives these variables on start and prepares the container.

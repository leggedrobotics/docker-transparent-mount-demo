#!/bin/bash

ROS_DISTRO=${1:-melodic}
shift

./build.sh $ROS_DISTRO

docker run -ti \
  --rm \
  --privileged \
  --net=host \
  -eHOST_UID=$(id -u) \
  -eHOST_GID=$(id -g) \
  -eHOST_HOME=$HOME \
  -eHOST_USERNAME=$(whoami) \
  -eHOST_GROUPNAME=$(id -gn) \
  -v $HOME:$HOME \
  -v$(pwd)/shadow:/etc/shadow \
  -v$(pwd)/passwd:/etc/passwd \
  -v$(pwd)/group:/etc/group \
  --name mount-demo-$ROS_DISTRO mount-demo-$ROS_DISTRO "$*"
